#include "rwmake.ch"
#include "topconn.ch"
#Include "Protheus.ch"
#Include "TbiConn.ch"

/*/{Protheus.doc} User Function curinga01
    (fun��o para copiar cadastros de clientes e contas a receber da empresa 010 para 040)
    @type  Function
    @author Andr? A. Alves
    @since 31/10/2019
    @version version
    @param param_name, param_type, param_descr
    @return return_var, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
/*/
User Function curinga01()

    Local aArea    := GetArea()
    Local cTitulo  := "Migracao do Cadastro de Clientes"
    Local nOpcao        := 0
    Local aButtons      := {}
    Local aSays         := {}
    Local cPerg         := "migcuringa"
    Private cArquivo    := ""
    Private oProcess
    Private lRenomear   := .F.
    
    If MsgYesNo("Deseja Copiar os dados da Empresa 010 para 040 ?")
        oProcess := MsNewProcess():New( { || CopyCli() } , "Migracao de registros " , "Aguarde..." , .F. )
        oProcess:Activate()
    EndIf
    RestArea(aArea)
Return

/*/{Protheus.doc} nomeStaticFunction
    (long_description)
    @type  Static Function
    @author user
    @since 05/11/2019
    @version version
    @param param_name, param_type, param_descr
    @return return_var, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
    /*/

Static Function CopyCli()
    
    Local aheader   := {}
    Local nCont     := 1

   IF Select("TMP") <> 0
        DbSelectArea("TMP")
        DbCloseArea()
    ENDIF

    _cquery:=" SELECT * FROM SA1010 A1"
    _cquery+="  WHERE A1.D_E_L_E_T_ = ' '"
    _cquery+=" AND (SELECT COUNT(*) "
    _cquery+="  FROM SE1010 E1 WHERE E1.D_E_L_E_T_ = ' ' "
    _cquery+=" AND A1_COD = E1_CLIENTE"
    _cquery+=" AND A1_LOJA = E1_LOJA"
    _cquery+=" AND E1_SALDO > 0) > 0"
    _cquery+=" AND A1_COD = '051934'"
    _cquery+=" AND A1_LOJA = '01'"
    _cquery+=" ORDER BY A1_COD, A1_FILIAL"

    _cquery:=changequery(_cquery)
    tcquery _cquery new alias "TMP"

    nTotReg := Contar("TMP","!Eof()")
    TMP->(DbGoTop())
    oProcess:SetRegua1(nTotReg)

    while !TMP->(eof())
 
        nCont++
        oProcess:IncRegua1("Migrando Clientes: " + Alltrim(Str(nCont)))

        sx3->(dbsetorder(1))
        sx3->(dbseek("SA1"))
        aCliente := {}

        while !sx3->(eof()) .and. sx3->x3_arquivo == "SA1"

            oProcess:SetRegua2(400)

            _cCpSA1 := "TMP->" + Alltrim(SX3->X3_CAMPO)
            _CampSeek := ("A1_MINIRF,A1_RECCSLL,A1_IRBAX,A1_TPPAGTO,A1_USADDA,A1_CODMUN,A1_RISCO,A1_MSBLQL,A1_BLOQCUR,A1_B2B")
            If sx3->x3_context != "V" .and. sx3->x3_tipo != "M" .and. !alltrim(sx3->x3_campo) $ _CampSeek
                oProcess:IncRegua2('Processando...: ' + ALLTRIM(SX3->X3_CAMPO))

                if sx3->x3_tipo == "D"
                    aadd(aCliente,{alltrim(sx3->x3_campo),ddatabase ,nil})
                Else
                    If sx3->x3_campo == "A1_COD"
                        aadd(aCliente,{alltrim(sx3->x3_campo),GetSXENum("SA1","A1_COD"),nil})
                    Else
                        aadd(aCliente,{alltrim(sx3->x3_campo),&(_cCpSA1),nil})
                    Endif
                EndIf
            EndIf
            sx3->(dbskip())
        end
        parada  := 0
        _Ok := ExecAutoCli(aCliente)

        If !_Ok
            ExecAutoTit(tmp->a1_cod, tmp->a1_loja)
        EndIf
        tmp->(dbskip())
    end

    //RpcClearEnv() 
Return()

/*/{Protheus.doc} nomeStaticFunction
    (long_description)
    @type  Static Function
    @author user
    @since 31/10/2019
    @version version
    @param param_name, param_type, param_descr
    @return return_var, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
    /*/
Static Function ExecAutoCli(aCliente)
    Private lMsErroAuto := .F.
    //PREPARE ENVIRONMENT EMPRESA '04' FILIAL '01'

    Begin Transaction

    MSExecAuto({|x,y| Mata030(x,y)},aCliente,3) //Inclusao   

    End Transaction

    If lMsErroAuto
        MostraErro()
        DisarmTransaction()
        Break
    else
        MsgInfo("Cliente cadastrado com sucesso!!")
        ConfirmSX8()
    endif
Return lMsErroAuto

/*/{Protheus.doc} P12TESTE2
    (long_description)
    @type  Static Function
    @author user
    @since 01/11/2019
    @version version
    @param param_name, param_type, param_descr
    @return return_var, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
    /*/
Static Function ExecAutoTit(_CodCli, _CodLoja)
    Private lMsErroAuto := .f.

    IF Select("TIT") <> 0
        DbSelectArea("TIT")
        DbCloseArea()
    ENDIF

    _cquer:=" SELECT * FROM SE1010 E1"
    _cquer+="  WHERE E1.D_E_L_E_T_ = ' '"
    _cquer+="  AND E1_CLIENTE = '"+_CodCli+"'"
    _cquer+="  AND E1_LOJA = '"+_CodLoja+"'"
    _cquer+="  AND E1_SALDO > 0"
    _cquer:=changequery(_cquer)
    tcquery _cquer new alias "TIT"
    
    sx3->(dbsetorder(1))
    sx3->(dbseek("SE1"))
    aTitulos    := {}
    
    while !TIT->(eof())
        while !sx3->(eof()) .and. sx3->x3_arquivo == "SE1"
            _cCpSE1 := "TIT->" + Alltrim(SX3->X3_CAMPO)
            If sx3->x3_context != "V"
                aadd(aTitulos,{alltrim(sx3->x3_campo),&(_cCpSE1),nil})
            EndIf
            sx3->(dbskip())
        end

        Begin Transaction

        MSExecAuto({|x,y| FINA040(x,y)},aTitulos,3) //Inclusao   

        End Transaction

        If lMsErroAuto
            MostraErro()
            DisarmTransaction()
            Break
        else
            MsgInfo("Titulo cadastrado com sucesso!!")
        endif

        tit->(dbskip())
    end
Return()

/*
Static Function ajustaSx1(cPerg)
    putSx1(cPerg, "01", "Arquivo"  , "", "", "mv_ch1", "C", 99, 0, 0, "G", "", "DIR", "", "","mv_par01", "", "", "", "", "", "", "","", "", "", "", "", "", "", "", "", {"Informe o arquivo TXT que ser?","importado (Extens?o CSV)",""}, {"","",""}, {"","",""})
    PutSx1(cPerg, "02", "Renomear?", "", "", "mv_ch2", "N",  1, 0, 2, "C", "",    "", "", "","mv_par02","Sim","Si","Yes","","Nao","No","No")
Return
*/