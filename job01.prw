#include "totvs.ch"
#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#Include "TbiConn.ch"


/*/{Protheus.doc} User Function job01
    (long_description)
    @type  Function
    @author user
    @since 01/11/2019
    @version version
    @param param_name, param_type, param_descr
    @return return_var, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
    /*/
User Function Job01()
    PREPARE ENVIRONMENT EMPRESA '04' FILIAL '00'

    aCliente :={;
    {"A1_COD"    	, "000015"                                                                		,Nil},; //Codigo     -C-06
    {"A1_LOJA"   	, "01"                                                           				,Nil},; //Loja       -C-02
    {"A1_NOME"   	, "cNome"				            			                     				,Nil},; //Nome       -C-40
    {"A1_NREDUZ" 	, "cNomRed"						                                 				,Nil},; //Nome Reduz.-C-20
    {"A1_MERCON" 	, "2"						                                 				,Nil},; //Nome Reduz.-C-20
    {"A1_END"    	, "cEndERECE"																			,Nil},; //Logradouro -C-40
    {"A1_BAIRRO" 	, "cBairro"														 				,Nil},; //Bairro     -C-30
    {"A1_LCRECOM" 	, 1000													 				,Nil},; //Bairro     -C-30
    {"A1_CONTRIB" 	, "1"														 				,Nil},; //Bairro     -C-30
    {"A1_PATVRJ" 	, "A"														 				,Nil},; //Bairro     -C-30
    {"A1_EST"    	, "DF"								                         				,Nil},; //Estado     -C-02
    {"A1_COD_MUN"	, "00108"														 				,Nil},; //Cod.Munic. -C-05
    {"A1_MUN"    	, "BRASILIA"										                       				,Nil},; //Cidade     -C-25
    {"A1_CEP"    	, "74310290"															 				,Nil},; //CEP        -C-08
    {"A1_PESSOA"  	, "J"																		,Nil},; //Tipo       -C-01 //F Final
    {"A1_CGC"    	, "20806890000130"                                          	 				,Nil},; //CPF-CNPJ   -C-14
    {"A1_TEL"    	, "0000"																			,Nil},; //Telefone   -C-15
    {"A1_PAIS"   	, "105"                                                          				,Nil},; //Cod.País   -C-05
    {"A1_EMAIL"  	, "andre@andre.com.br"																		,Nil},; //ContaCont. -C-20
    {"A1_TIPO"   	, "F"                                                            				,Nil},; //Tipo = F - Cons.Final
    {"A1_CODPAIS"	, "01058"                                                        				,Nil} } //Cod.País   -C-05
    
    lMsErroAuto := .F.
    //PREPARE ENVIRONMENT EMPRESA '04' FILIAL '01'

    Begin Transaction

    MSExecAuto({|x,y| Mata030(x,y)},aCliente,3) //Inclusao   

    End Transaction

    If lMsErroAuto
        MostraErro()
        DisarmTransaction()
        Break
    else
        MsgInfo("Cliente cadastrado com sucesso!!")
        ConfirmSX8()
    endif
Return()